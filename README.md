fixed\_join.vim
===============

This plugin provides a mapping target to `:join` lines in normal mode while
keeping the cursor in place, and still supporting a count prefix.

License
-------

Copyright (c) [Tom Ryder][1]. Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
