"
" fixed_join.vim: User-defined key mapping and optional command to keep cursor
" in place when joining lines in normal mode.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('g:loaded_fixed_join') || &compatible
  finish
endif
if v:version < 600
  finish
endif
let g:loaded_fixed_join = 1

" Declare function
function! s:FixedJoin()

  " Save current cursor position
  let l:cursor_line = line('.')
  let l:cursor_col = col('.')

  " Find last line of join; silently cap it to last buffer line
  let l:join_line = l:cursor_line + v:count1
  let l:last_line = line('$')
  if l:join_line > l:last_line
    let l:join_line = l:last_line
  endif

  " Build and execute join command
  let l:command = l:cursor_line . ',' . l:join_line . 'join'
  execute l:command

  " Return the cursor to the saved position (Vim 6.0 fallback)
  if exists('*cursor')
    call cursor(l:cursor_line, l:cursor_col)
  else
    execute 'normal! '
          \ . l:cursor_line . 'G'
          \ . l:cursor_col . '|'
  endif

endfunction

" Create modeless mapping target for the function just defined
noremap <silent>
      \ <Plug>FixedJoin
      \ :<C-U>call <SID>FixedJoin()<CR>
